--Set WiFi to proper mode and make it a friendly SSID
wifi.setmode(wifi.STATIONAP)
wifi.ap.config({ssid="BBQ_Probe", auth=wifi.OPEN})

function startup()
    if file.open("init.lua") == nil then
        print("init.lua deleted or renamed")
    else
        print("Running")
        -- the actual application is stored in 'app.lua'
        require("app")
        start_app()
        file.close("init.lua")
    end
end

--Keep track of failed connection attemps
count = 0

tmr.alarm(1, 3000, 1, function()
    --did we connect to a known WiFi AP?
    if wifi.sta.getip() == nil then
        --No
        print("Waiting for IP address...")
        count = count + 1
        if count > 10 then
            --Too many failed retries
            tmr.stop(1)
            tmr.unregister(1)
            print("Starting enduser setup...")
            enduser_setup.manual(true)
            enduser_setup.start(
                function()
                    print("New WiFi added")
                    print("Connected to wifi as: " .. wifi.sta.getip())
                    enduser_setup.stop()
                    --register with dns on local network 
                    mdns.register("bbq_probe", { description="Smoker & BBQ Probe", service="http", port=80 })
                    startup()
                end,
                function()
                --Not sure how to get this to trigger so currently only works with WiFi, including phone hotspots
                    print("Wifi connection not successful, ad-hoc enabled")
                    enduser_setup.stop()
                    wifi.setmode(wifi.SOFTAP)
                    startup()
                end)
                
        end
    else
        --Yes
        print("Using saved network")
        print("Connected to wifi as: " .. wifi.sta.getip())
        tmr.stop(1)
        tmr.unregister(1)
        --register with dns on local network 
        mdns.register("bbq_probe", { description="Smoker & BBQ Probe", service="http", port=80 })
        startup()
    end
end)
