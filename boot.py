import network
import webrepl
import gc

def wifi_connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print("connecting to network...")
        sta_if.active(True)
        sta_if.connect("some_ssid", "some_password")
        while not sta_if.isconnected():
            pass
            
    print("network config:", sta_if.ifconfig())

wifi_connect()
webrepl.start()
gc.collect()
