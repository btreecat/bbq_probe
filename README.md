# Wifi Enabled BBQ Probe
#### Quick access to your smoker/bbq temp from anywhere with internet!

## Hardware

The hardware requirements for this project are very minimal and thanks to competition, there are several options to choose from in terms of venders and specific chips and parts

* Food grade thermister based temp probe
* An appropriate value resistor
* ESP8266 based MCU if you want to use the code provided, other platforms should be trivial to port/support
* Battery power
* A seperate thermomiter for verification/calibration
* Soldering iron/solder and some sort of insulator
* WiFi network 
* Multimeter 
* Lighter

### Temp Probe

The temp probe I used was harvested from a broken wireless based thermomiter purchased at Bed Bath & Beyond. The wireless on it was awful, the reciving unit bearly reached 10 meters range. The unit was eventually destroyed by the elements (aka left outside) and the probe was harvested. 

Temp probes from just about any cheap digial thermomiter should work, but ones with the electronics built onto the shaft might be more difficult to work with. 

There are also plenty of options on Amazon or other online vendors. 

* [Cheap probe from Amazon] (https://www.amazon.com/Famili-TPW01-Stainless-Replacement-Temperature/dp/B01FD8SAUA/)
* [Another Amazon Option] (https://www.amazon.com/Polder-358-Ultra-Probe/dp/B00NGWKXWE/ref=sr_1_14?ie=UTF8&qid=1475852146&sr=8-14&keywords=probe+thermometer)
* [Here is one on Ebay] (http://www.ebay.com/itm/Maverick-Stainless-Steel-Replacement-Food-Probe-3-/331992016462?hash=item4d4c44264e:g:aQwAAOSwgtJXThcf)

The thing about these is that they likely have unknown values for their [Steinhart-Hart constants)[https://en.wikipedia.org/wiki/Steinhart%E2%80%93Hart_equation] just like mine did. So in order to make them useful, you need to have an idea of the range of resistance values capable from the probe. This will let you pick an appropriate resistor based on the chip you chose. 

In order to get this info, we will want to "freeze" and "burn" the probe to get an idea of some extreme values. We will also need 3 known values for calibration but that can be done after. 

1. Using either a styrafoam cup or ceramic cup with some insulation, fill it with ice to the top, and about 80% up with water and stir.
2. Using the probe connected to a multimeter to read the resistance, stire the ice water with the probe vigerously, then let it sit in the water for a few seconds. This should produce a value very close to 0C. Depending on the type of probe used this will either be the highest or the lowsest resistance you will read. For my probe it was the highest, at around 333000 kOhms. Save this for later as we will need it for your S-H constants.
3. Using a lighter, heat the tip of the probe for about 10 seconds, this should represent the max temp your bbq/smoker is likely to reach (although if you get direct flame on the probe you should be reducing your flames or moving the probe!). Record the resistance read, for me this was the lowest value around 400 Ohms (notice how wide a range that is).

Now that you know your ranges, we can use our extra thermomiter to verify our temps and gather 3 resistance values at known tempatures. You want these temps to be somewhat spaced apart, so freezing water, room temp and boiling water make good points of meaturement. 

1. Get the resistance at room temp
2. Get the resistance at "freezing" water using the method mentioned above
3. Get the resistance at boiling water

Double check these values or better yet, take an avgerage of 3-5 readings.

Use these temp-resistance value pairs to fill out the [S-H calculator](http://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm) to get your constants for the S-H equation. 

Save these values for later because they will be needed in the source code

### Resistor

So with these recorded values we can do a little math and figure out what resistor we need to make a [voltage divider](https://learn.sparkfun.com/tutorials/voltage-dividers) that will output reasonably accurate values for the temp ranges we want. The ranges however will be directly depended on the chip used as the ADC input range varies.  

You will want to test the exact resistor used a few times and take an average of the values from the multimeter. This will give you a more accurate model when calculating the resistance of the thermistor at any given time. 

For my probe, I used a 2.2 kOhm reistor that measured at 2.168 kOhms avergage. I used a cheap ristor with a 5% tolerance but for furture ones I will switch to a better quality ones. 

### ESP8266 or NodeMCU Dev Kit

The heart of this project is the [ESP8266 WiFi MCU.] (https://en.wikipedia.org/wiki/ESP8266) This little chip (on certain versions) has a built in [ADC](https://en.wikipedia.org/wiki/Analog-to-digital_converter) with a single pin good enough for one temp probe. The main issue is knowing what the input range is on the chip. The ESP8266 7/12 chips seem to be only 0.0-1.0v where as some of the NodeMCU Dev Kit boards are 3.3v tolerant. 

Do lots of research and know that if you are wrong, you could fry the board. Fortunately they are inexpensive to replace or buy extra. 

Suitable boards include:

* [LoLin "v3" (3.3v ADC)] (http://www.banggood.com/V3-NodeMcu-Lua-WIFI-Development-Board-p-992733.html?rmmds=search)
* [Geekcreit Doit] (http://www.banggood.com/Geekcreit-Doit-NodeMcu-Lua-ESP8266-ESP-12E-WIFI-Development-Board-p-985891.html?rmmds=search)
* [D1 Mini OG (3.3v ADC)] (http://www.banggood.com/D1-Mini-NodeMcu-Lua-WIFI-ESP8266-Development-Board-p-1044858.html?rmmds=search)
* [D1 Mini Clone](http://www.banggood.com/Mini-NodeMCU-ESP8266-WIFI-Development-Board-Based-On-ESP-12F-p-1054209.html?rmmds=search)
* [ESP-12F (just MCU + WiFi, no USB, 1v ADC)](http://www.banggood.com/5Pcs-ESP8266-ESP-12F-Remote-Serial-Port-WIFI-Transceiver-Wireless-Module-p-1046654.html?rmmds=search)

I prefer the ones with USB built in since it allows for quick adjustmints and 5v input for power. The WeMos D1 Mini is a great choice since it is much smaller than the LoLin that I used, and is known the be 3.3v ADC. Not sure about the clone, and the ESP-12 chips are 1v ADC. Also if you go with an ESP-12 chip, you will need an FTDI programming tool. The USB models include a USB-to-serial interface. 

### Battery

I am going to use a spare USB phone power pack since they are trivial to recharge and have plenty of power for hours of tempature monitoring. 

All of the USB based models are going to be easier to power since they have built in regulators. The ESP-12 chips require 3v input so two AA battieres, or a single cell lipo should make good choices. 

## Software

### Fimrware

I used [ESplorer](http://esp8266.ru/esplorer/) to manage the files, communicate via REPL, and make sure everything worked. 

Also you will need [esptool](https://github.com/themadinventor/esptool) to flash the latest firmware.

Firmware build and flashing instructions can be found here:

* https://nodemcu.readthedocs.io/en/master/en/flash/
* https://nodemcu-build.com/

When building the firmware, make sure you enable: 

* ADC
* end user setup
* file
* net
* node
* timer
* UART
* Wifi

Everything else can be diabled to save space and memory.

I had to also flash the esp_init_data_default.bin file in order to boot.

### Application

Once you have the firmware flashed, you need to get a copy of the lua files required to make it all work. 

https://gitlab.com/btreecat/bbq_probe

The only files you will need for the NodeMCU/eLua firmware are the .lua files. The Python files are for MicroPython if you wish to experiment with that envirornment. I personally found the tooling to be less mature and the enviornment a bit too heavy. 

* app.lua
* init.lua
* natual_log.lua

Once you clone or download the files, you can open them in ESplorer, and edit the appropriate constants in app.lua at the top of the file. 

The values you need to change are 

```
--The measured resistance of the fixed resistor in the voltage div 
FIXED_R = 2168 

--Make sure your change these based on your own calculations and thermistor used
Ash = 0.0003859537760       
Bsh = 0.0002584871859       
Csh = -0.000000005767406068      
```

Once you chnage those values to match your hardware used, you are ready to upload the files to the board. 

Upload them in this order: 

1. natual_log.lua
2. app.lua
3. init.lua

Now power cycle the chip and noice with a WiFi device like a cellphone that a new network will appear called "BBQ_Probe" and you will want to connect to this network. Your device may complain about no internet access but just ignore it. Next open a browser and go to [http://192.168.4.1](http://192.168.4.1) and you should be greeted with a page to configure access to your WiFi. If you are using it out and about, you will need to enable hotspot on your mobile device and set the probe to join
that network. 

Once your probe joins the network, it should give you a message at the bottom of the page with the IP address it received. Rejoin your home WiFi and attempt to access that address in a browser. You can configure your home router to assing the same address every time to make it easier to locate the device on your network. If you are having trouble locating the device, check your routers admin page as there is almost always a place that lists the connected clients. 

With the IP address now known, putting that address in the browser will caus it to start showing the temp and should auto refresh every 5 seconds. You should also be able to connect directly to the "BBQ Probe" WiFi again and monitor the temp from the 192.168.4.1 address. 

#### Share and Enjoy!
