--The measured resistance of the fixed resistor in the voltage div 
FIXED_R = 2168      --CHANGEME


--Constants derrived through experimentation and S-H calculator 
--http://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm
--Make sure your change these based on your own calculations and thermistor used
Ash = 0.0003859537760       --CHANGEME
Bsh = 0.0002584871859       --CHANGEME
Csh = -0.000000005767406068     --CHANGEME 


--The Max possible ADC value
MAX_ADC = 1024


--NodeMCU's version of eLua lacks a math.log() function so I had to write one
require("natural_log")


function get_probe()
    --Open the ADC pin and read it's current value between 0-1023
    return adc.read(0)
end

function get_resist()
    --Use the current ADC value to calculate the resistance of the thermistor
    adc_value = 0
    --Take an average of values with short delays between each 
    for i=0,9,1 do
        adc_value = adc_value + get_probe()
        tmr.delay(10)
    end
    adc_value = adc_value / 10
    --return an int since it makes ln() easier to implement
    return math.floor((MAX_ADC - adc_value) * FIXED_R / adc_value)
    
end

function get_temp()
    --Using the calculated resistance, solve for K temp and convert to C temp
    avg_r = 0
    --Take an average of values with short delays between each 
    for i=0,9,1 do
        avg_r = avg_r + get_resist()
        tmr.delay(10)
    end
    avg_r = avg_r / 10
    --S-H eqaution returns temp in K based on thermistor resistance and known constants Ash, Bsh, Csh
    --https://en.wikipedia.org/wiki/Steinhart%E2%80%93Hart_equation
    k_temp = 1 / ( Ash + Bsh * ln(avg_r) + Csh * math.pow(ln(avg_r), 3))
    return k_temp - 273
end
    
function get_html()
    --Tuncate to 2 decimal place max
    c_temp = tonumber(string.format("%.2f",get_temp()))
    
    --create a table of text to make concatination faster
    html = {}

    table.insert(html, [[
        <html>
            <head>
                <title>WiFi Temp Probe 
                ]])
    table.insert(html, c_temp)
    table.insert(html,   [[
                </title>
                <meta http-equiv="refresh" content="5">
            </head>
            <body>
                <h1 style="font-size: 12vw;">Current Temp: </br> 
                ]])
    table.insert(html, c_temp)
    table.insert(html,   [[
                C</h1>
            </body>
        </html>
        ]])
    
    return table.concat(html)
end

function start_http()
    --Only one server per port
    httpd = net.createServer(net.TCP)
    httpd:listen(80, function(conn)
        conn:on("receive", function(conn, payload)
            --Payload contains enough info to parse out additional request info
            print(payload)
            conn:send(get_html())
        end)
        conn:on("sent",function(conn) conn:close() end)
    end)
end


function start_app()
    start_http()
end
    
