import machine
import math
import socket
import network
import gc

FIXED_R = 2168
MAX_ADC = 1024

Ash = 0.0003859537760
Bsh = 0.0002584871859
Csh = -0.000000005767406068


def get_probe(): 
    #Open the ADC pin and return it's current value
    return machine.ADC(0).read()


def get_resist():
    #Using the probed value, solve for the thermistor value
    adc = get_probe()
    return (1023 - adc) * FIXED_R / adc


def get_temp():
    #Using the value of R1, solve for the K temp and convert to C
    r1 = get_resist()
    
    k_temp =  1 / ( Ash + Bsh * math.log(r1) + Csh * pow(math.log(r1), 3) )
    
    return k_temp - 273


def get_html():

    c_temp = round(get_temp(), 2)
    
    html =  """
        <html>
            <head>
                <title>WiFi Temp Probe {c_temp}</title>
                <meta http-equiv="refresh" content="5">
            </head>
            <body>
                <h1>Current Temp: {c_temp}</h1>
            </body>
        </html>
        """
    html = html.format(c_temp=c_temp)

    return html


def start_http():
    sta_if = network.WLAN(network.STA_IF)
    #print("network config:", sta_if.ifconfig())
    addr = socket.getaddrinfo("0.0.0.0", 80)[0][-1]
    s = socket.socket()
    s.bind(addr)
    s.listen(1)
    
    #print("listening on", addr)

    while True:
        conn, addr = s.accept()
        #print("conn is: ", conn)
        #print("client connected from", addr)
        conn_file = conn.makefile("rwb", 0)
        while True:
            line = conn_file.readline()
            if not line or line == b"\r\n":
                gc.collect()
                break
        html = get_html()
        conn.send(html)
        conn.close()

start_http()
